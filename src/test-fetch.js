import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planets:{type:Object}
    };
  }

  constructor() {
    super();
    this.planets={results:[]};
  }

  render() {
    return html`
      ${this.planets.results.map((pl)=>{
          return html`<div>${pl.name} ${pl.rotation_period}</div>`
      })}
    `;
  }

  connectedCallback(){
      super.connectedCallback();
      try{
          this.cargarPlaneta();
      }catch(e){
          alert(e);
      }
  }
  cargarPlaneta(){
    fetch("https://swapi.dev/api/planets/")
    .then(response=>{
        console.log(response);
        if(!response.ok){throw response;}
        return response.json();
    })
    .then(data=>{
        this.planets=data;
        console.log(data);
    })
    .catch(error=>{
        alert("Problema con el fetch"+error);
    })
  }
}

customElements.define('test-fetch', TestFetch);