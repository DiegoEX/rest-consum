const {When,Then,After}=require('@cucumber/cucumber');
const assert=require('assert');
const {Builder,By,until}=require('selenium-webdriver');
const chromedriver=require('chromedriver');
const chrome=require('selenium-webdriver/chrome');

When('we request the products list', async function(){
    
    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
    this.driver=new Builder()
    .forBrowser('chrome')
    .build();
    this.driver.wait(until.elementLocated(By.className('products-list')));
    await this.driver.get('http://192.168.1.73:8000');

})

Then('we should receive',async function(dataTable){
    var productElements=await this.driver.findElements(By.className('producto'));
    var expectations =dataTable.hashes();
    for(let i=0;i<expectations.length;i++){
        const productName =await productElements[i].findElement(By.css('h3')).getText();
        assert.strictEqual(productName,expectations[i].nombre);
        const productDesc =await productElements[i].findElement(By.css('p')).getText();
        assert.strictEqual(productDesc,expectations[i].descripcion);

    }

})

After(async function(){
    this.driver.close();
})
