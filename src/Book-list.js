import { LitElement, html, css } from 'lit-element';

export class BookList  extends LitElement {

  static get styles() {
    return css`
  

    `;
  }

  static get properties() {
    return {
      resultados:{type: Object}
        
    };
  }

  constructor() {
    super();
       this.resultados={"books":[]};
       this.cargaDatos("http://192.168.1.73:3392/books");
  
  }

  render() {
    return html`
    <div>
    ${this.resultados.books.map(b=>html`${b.titulo} ${b.autor}`)}
    </div>

    `;
  }
  

  cargaDatos(url){
      fetch(url)
      .then(response=>{
          console.log(response);
          if(!response.ok){throw response;}
          return response.json();
      })
      .then(data=>{
          this.resultados=data;
          console.log(data);
      })
      .catch(error=>{
          alert("Problema con el fetch"+error);
      })
  }
 

}

customElements.define('book-list', BookList);