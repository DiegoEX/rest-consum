import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {type:Array}
    };
  }

  constructor() {
    super();
    this.productos=[];
    this.productos.push({"nombre":"Movil XL","descripcion":"Un telefono con  una de las mejores pantallas"});
    this.productos.push({"nombre":"Movil Mini","descripcion":"Un telefono muy pequeño"});
    this.productos.push({"nombre":"Movil Standart","descripcion":"Un telefono estandar.Nada especifico"});
    
  }

  render() {
    return html`
      <div class="contenedor">
        ${this.productos.map(p=>html`<div class="producto"><h3>${p.nombre}</h3><p>${p.descripcion}</p></div>`)}
      </div>
    `;
  }

  createRenderRoot(){
      return this;//se saca del shadow root para qeu el test pueda acceder
  }
}

customElements.define('products-list', ProductsList);