import { LitElement, html, css } from 'lit-element';

export class TestOpentable  extends LitElement {

  static get styles() {
    return css`
    table{
        width:500px;
        font:normal 13px Arial;
        text-align:center;
        border-collapse:collapse;
        border: 1px solid #000;
     }
      th {
        font:bold 15px Arial;
        background-color:lightblue;
        border: 1px solid #000;
        border-spacing: 0;
    }
    button {
        float:left;
    }

    `;
  }

  static get properties() {
    return {
        resultados:{type: Object},
        campos:{type: Array},
        filas:{type: Array},
        sig:{type:Boolean},
        prev:{type:Boolean},
        entidades:{type:Array}
    };
  }

  constructor() {
    super();
    this.campos=[];
    this.resultados={results:[]};
    this.filas=[];
    this.cargaDatos("https://swapi.dev/api/planets/");
    this.entidades=["planets","films","people","species","startships","vehicles"]
  }

  render() {
    return html`
    <div><select id="sel" @change="${this.changeSel}">
    ${this.entidades.map(e=>html`<option>${e}</option>`)}
    </select>
    </div>
        <table width="100%" >
            <tr>
                ${this.campos.map(c=>html` <th>${c}</th>`)}
            </tr>
            ${this.filas.map(i=> html`<tr>
                ${this.resultados.results[i].valores.map(v=>html`<td >${v}</td>`)}
            </tr>`)}
        </table>
        ${this.prev?html` <div><button @click="${this.anterior}">Anterior</button></div>`:html``}
        ${this.sig?html` <div><button @click="${this.siguiente}">Siguiente</button></div>`:html``}
       
    
    `;
  }
  changeSel(){
      var entidad=this.shadowRoot.querySelector("#sel").value;
      var url="https://swapi.dev/api/"+entidad+"/";
      this.cargaDatos(url);

  }

  cargaDatos(url){
      fetch(url)
      .then(response=>{
          console.log(response);
          if(!response.ok){throw response;}
          return response.json();
      })
      .then(data=>{
          this.resultados=data;
          this.montraResultados();
      })
      .catch(error=>{
          alert("Problema con el fetch"+error);
      })
  }
  anterior(){
      this.cargaDatos(this.resultados.previous);
  }

  siguiente(){
    this.cargaDatos(this.resultados.next);
  }

  
  montraResultados(){
      this.campos=this.getObjProps(this.resultados.results[0]);
      this.filas =[];
      for(var i=0;i<this.resultados.results.length;i++){
          this.resultados.results[i].valores=this.getValores(this.resultados.results[i]);
          this.filas.push(i);
      }
      this.sig=(this.resultados.next !==null);
      this.prev=(this.resultados.previous!==null);

  }
  getValores(fila){
      var valores =[];
      for(var i=0;i<this.campos.length;i++){
          valores[i]=fila[this.campos[i]];
      }

      return valores;
  }

  getObjProps(obj){
      var props=[];
      for(var prop in obj){
          if(!this.isArray(this.resultados.results[0][prop])){
              if((prop!=="created")&&(prop!=="edited")&&(prop!=="url")){
                  props.push(prop);
              }
          }
      }
      return props;

  }

  isArray(prop){
      return Object.prototype.toString.call(prop)==='[Object Array]';
  }
}

customElements.define('test-opentable', TestOpentable);