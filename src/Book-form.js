import { LitElement, html, css } from 'lit-element';

export class BookForm  extends LitElement {

  static get styles() {
    return css`
  

    `;
  }

  static get properties() {
    return {
        data:{type: Object},
        id:{type:Number},
        titulo:{type:Text},
        autor:{type:Text}
        
    };
  }

  constructor() {
    super();
       this.id=0;
       this.titulo="";
       this.autor="";
  
  }

  render() {
    return html`
    <div>
        <label for="iid">ID</label>
        <input type="number" id="iid" .value="${this.id}" @input="${this.updateId}"/>
        <br/>
        <label for="ititulo">ID</label>
        <input type="text" id="ititulo" .value="${this.titulo}" @input="${this.updateTitulo}"/>
        <br/>
        <label for="iautor">ID</label>
        <input type="text" id="iautor" .value="${this.autor}" @input="${this.updateAutor}"/>
        <br/>
        <button @click="${this.buscarBook}">Buscar</button>
        <button @click="${this.crearBook}">Crear</button>
        <button @click="${this.modificarBook}">Modificar</button>
        <button @click="${this.eliminarBook}">Eliminar</button>
    </div>

    `;
  }

  updateId(e){
      this.id=parseInt(e.target.value);
  }
  updateTitulo(e){
    this.titulo=e.target.value;
  }   
  updateAutor(e){
        this.autor=e.target.value;
  }
  modificarBook(){
    var book=this.getCurrentBook();
    const option={
        method:'PUT',
        body: JSON.stringify(book),
        headers:{'Content-Type':"application/json"}
    };
    fetch("http://localhost:3392/books",option)
    .then(response=>{
        console.log(response);
        if(!response.ok){throw response;}
        return response.json();
    })
    .then(data=>{
       alert("Book Modificado");
        
    })
    .catch(error=>{
        alert("Problema con el fetch"+error);
    })

  }
  eliminarBook(){
    var book=this.getCurrentBook();
    const option={
        method:'DELETE',
        body: JSON.stringify(book),
        headers:{'Content-Type':"application/json"}
    };
    fetch("http://localhost:3392/books/"+this.id,option)
    .then(response=>{
        console.log(response);
        if(!response.ok){throw response;}
        return response.json();
    })
    .then(data=>{
       alert("Book eliminado");
        
    })
    .catch(error=>{
        alert("Problema con el fetch"+error);
    })

  }

  getCurrentBook(){
      var book={};
      book.id=this.id;
      book.titulo=this.titulo;
      book.autor=this.autor;
      return book;
  }

  crearBook(){
      var book=this.getCurrentBook();
      const option={
          method:'POST',
          body: JSON.stringify(book),
          headers:{'Content-Type':"application/json"}
      };
      fetch("http://localhost:3392/books",option)
      .then(response=>{
          console.log(response);
          if(!response.ok){throw response;}
          return response.json();
      })
      .then(data=>{
         alert("Book creado");
          
      })
      .catch(error=>{
          alert("Problema con el fetch"+error);
      })
  }
  

  buscarBook(){
      fetch("http://localhost:3392/books/"+this.id)
      .then(response=>{
          console.log(response);
          if(!response.ok){throw response;}
          return response.json();
      })
      .then(data=>{
          this.id=data.id;
          this.titulo=data.titulo;
          this.autor=data.autor;
          console.log(data);
          
      })
      .catch(error=>{
          alert("Problema con el fetch"+error);
      })
  }
 

}

customElements.define('book-form', BookForm);